using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(MeshFilter))]
public class MeshGenerator : MonoBehaviour
{
    
    private Mesh _mesh;

    private Vector3[] _vertices;
    private int[] _triangles;

    [SerializeField] private int _width;
    [SerializeField] private int _height;


    void Start()
    {
        
        _mesh = new Mesh();
        GetComponent<MeshFilter>().mesh = _mesh;

        CreateShape();
        UpdateShape();

    }

    private void CreateShape()
    {
        _vertices = new Vector3[(_width+1)*(_height+1)];
        int i = 0;
        for (int z = 0; z <= _height; z++)
        {
            for (int x = 0; x <= _width; x++)
            {
                _vertices[i] = new Vector3(x,0,z);
                i++;
            }

        }
        //_triangles = new int[_width*_height*2 * 3];
        _triangles = new int[_width * _height * 3 *2];

        int vert = 0;
        int tris = 0;
        for (int z =0;z<_height ;z++)
        {
            for (i = 0; i < _width; i++)
            {
                int n = tris;
                Debug.Log(n);
                _triangles[n] = vert;
                _triangles[n + 1] = vert + _width + 1;
                _triangles[n + 2] = vert + 1;

                _triangles[n + 3] = vert + _width + 1;
                _triangles[n + 4] = vert + _width + 2;
                _triangles[n + 5] = vert + 1;

                vert++;

                tris += 6;
            }
            vert++;
        }
        
        
        
    }

    

    private void UpdateShape()
    {
        _mesh.Clear();
        _mesh.vertices = _vertices;
        _mesh.triangles = _triangles;
        _mesh.RecalculateNormals(); 
    }
    private void OnDrawGizmos()
    {
        if (_vertices ==null)
        {
            return;
        }
        for (int i = 0; i < _vertices.Length; i++)
        {
            Gizmos.DrawSphere(_vertices[i], .1f) ;
        }
    }
}
