using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class MapDisplay : MonoBehaviour
{
    public Renderer mapTexture;
    public MeshFilter filter;
    public MeshRenderer meshRenderer;
    public MeshCollider meshCollider;

    public void DrawTexture(Texture2D tex)
    {
        mapTexture.sharedMaterial.mainTexture = tex;
        mapTexture.transform.localScale = new Vector3(tex.width, 1, tex.height);
    }

    public void DrawMesh(MeshInfo meshInfo, Texture2D tex)
    {
        filter.sharedMesh = meshInfo.CreateMesh();
        meshRenderer.sharedMaterial.mainTexture = tex;
        meshCollider.sharedMesh = filter.sharedMesh;
    }
}
