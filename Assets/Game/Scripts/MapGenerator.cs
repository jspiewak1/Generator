using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour
{
    public enum MapType {Noise, Color, Terrain, NoiseTerrain };
    public MapType mapType;

    public const int _mapChunkSize = 241;
    [Range(0,6)]
    public int levelOfDetail;
    [SerializeField] private float _mapScale;
    [SerializeField] private float _mapHeightMultiplier; //ten prawdziwy mapHeight
    [SerializeField] private AnimationCurve meshHeightCurve;
    [SerializeField] private bool _isflattening;
    [Range(0, 1)]
    [SerializeField] private float _flatLevel;

    [SerializeField]private int _octaves;
    [Range(0, 1)]
    [SerializeField]private float _persistance;
    [SerializeField] private float _lacuranity;
    
    
    [SerializeField] private int _seed;
    [SerializeField] private Vector2 _offset;

    [SerializeField] private ModelGeneration _md;

    public bool noiseUpdate;

    public TerrainType[] tt;  

    

    public void GenerateNoiseMap()
    {
        float[,] noiseMap = Noise.GenerateNoiseMap(_mapChunkSize,_mapChunkSize, _mapScale, _seed , _octaves, _persistance, _lacuranity, _offset, _isflattening, _flatLevel);
        Color[] mapColor = new Color[MapArea()];
        for (int y = 0; y < _mapChunkSize ;y++)
        {
            for (int x = 0;x< _mapChunkSize ; x++)
            {
                float currentHeight = noiseMap[x,y];
                for (int i = 0;i< tt.Length ; i++)
                {
                    if (currentHeight<= tt[i].height)
                    {
                        mapColor[_mapChunkSize * y + x] = tt[i].color;
                        break;
                    }
                }
            }
        }
        
        MapDisplay mapDisplay = GetComponent<MapDisplay>();
        switch (mapType)
        {
            case MapType.Noise:
                mapDisplay.DrawTexture(TextureGenerator.NoiseMapTexture(noiseMap));
                break;
            case MapType.Color:
                mapDisplay.DrawTexture(TextureGenerator.ColorMapTexture(mapColor,_mapChunkSize, _mapChunkSize));
                break;
            case MapType.Terrain:
                mapDisplay.DrawMesh(MeshCreator.GenerateTerrainMesh(noiseMap, _mapHeightMultiplier, meshHeightCurve, levelOfDetail), TextureGenerator.ColorMapTexture(mapColor, _mapChunkSize,_mapChunkSize ));
                
                break;
            case MapType.NoiseTerrain:
                mapDisplay.DrawMesh(MeshCreator.GenerateTerrainMesh(noiseMap, _mapHeightMultiplier, meshHeightCurve, levelOfDetail), TextureGenerator.NoiseMapTexture(noiseMap));
                break;
        }
        

    }

    private int MapArea()
    {
        return _mapChunkSize*_mapChunkSize;
    }

    private void OnValidate()
    {
        if (_lacuranity <1)
        {
            _lacuranity = 1;
        }
        if (_octaves <0)
        {
            _octaves = 0;
        }


    }
}
[System.Serializable]
public struct TerrainType
{
    public string name; 
    public float height;
    public Color color;
}