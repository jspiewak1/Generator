using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextureGenerator 
{
    public static Texture2D ColorMapTexture(Color[] colorMap,int width, int height )
    {
        Texture2D texture = new Texture2D(width,height);
        texture.filterMode = FilterMode.Point;
        texture.wrapMode = TextureWrapMode.Clamp;  

        texture.SetPixels(colorMap);
        texture.Apply();
        return texture;
    }

    public static Texture2D NoiseMapTexture(float[,] noiseMap)
    {
        int width = noiseMap.GetLength(0);
        int height = noiseMap.GetLength(1);

        
        Color[] noiseColors = new Color[width * height];

        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                noiseColors[x + y * width] = Color.Lerp(Color.black, Color.white, noiseMap[x, y]);
            }
        }
        return ColorMapTexture(noiseColors,width,height );
    }
}
