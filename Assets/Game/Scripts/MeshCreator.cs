using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UIElements;

public static class MeshCreator 
{
    public static MeshInfo GenerateTerrainMesh(float[,] heightMap, float mapHeight, AnimationCurve heightCurve, int levelOfDetal )
    {
        int width = heightMap.GetLength(0);
        int height = heightMap.GetLength(1);

        if (levelOfDetal == 0)
            levelOfDetal = 1;

        int meshSimplification = levelOfDetal*2;
        int verticesPerLine = (width - 1) / meshSimplification + 1;

        MeshInfo meshInfo = new MeshInfo(verticesPerLine, verticesPerLine);
        int pointIndex = 0;
        for (int y=0;y<height ;y+= meshSimplification) 
        {
            for (int x=0;x<width ;x+= meshSimplification)
            {
                meshInfo.points[pointIndex] = new Vector3(x, heightCurve.Evaluate(heightMap[x, y]) *mapHeight,y);
                meshInfo.uv[pointIndex] = new Vector2(x /(float)width, y/(float)height);


                if (x < width -1 && y<height -1 )
                {
                    meshInfo.AddTriangle(pointIndex, pointIndex+ verticesPerLine + 1, pointIndex + verticesPerLine);
                    meshInfo.AddTriangle(pointIndex, pointIndex+1, pointIndex + verticesPerLine + 1);
                }
                
                pointIndex++;
            }
        }


        //for (int y = 0;y<height ;y++)
        //{
        //    for (int x = 0;x<width ; x++)
        //    {

        //    }
        //}
        return meshInfo;
    }
}
public class MeshInfo
{
    public Vector3[] points;
    public int[] triangles;
    public Vector2[] uv;



    int triangleIndex;

    public MeshInfo(int x, int y) //x, y are width and height
    {
        points = new Vector3[x * y];
        uv = new Vector2[x * y];

        triangles = new int[(x-1)*(y-1)*6];
        
    }

    public void AddTriangle(int a, int b, int c)
    {
        triangles[triangleIndex] = a;
        triangles[triangleIndex + 1] = b;
        triangles[triangleIndex + 2] = c;
        triangleIndex += 3;

    }

    public Mesh CreateMesh()
    {
        Mesh mesh = new Mesh();
        mesh.vertices = points;
        mesh.triangles = triangles;
        mesh.uv = uv;
        mesh.RecalculateNormals();
        return mesh;
    }

}