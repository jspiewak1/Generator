using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelGeneration : MonoBehaviour
{
    [SerializeField] private GameObject cube;
    private RaycastHit _hit;
    private Ray _ray;
    public ObjectType[] ot;
    void Start()
    {
        GenerateRandom();
        Vector3 offset = new Vector3(120, -50, 120);
        //GameObject go = Instantiate(cube, this.gameObject.transform);
        //go.transform.localPosition = offset;
    }
    private void GenerateRandom()
    {
        for (int i = 0;i< ot.Length; i++)
        {
            int j = 0;
            while (ot[i].amount > j)
            {
                Debug.Log("kurwa");
                Vector3 fwd = this.transform.TransformDirection(Vector3.up);
                if (Physics.Raycast(this.transform.position, fwd, out _hit, 100) && _hit.distance < ot[i].maxDistance && _hit.distance > ot[i].minDistance)
                {
                    this.transform.localPosition = new Vector3(Random.Range(1,239), -50, Random.Range(1, 239));
                    Debug.DrawRay(this.transform.position, fwd * 50, Color.green);
                    GameObject go = Instantiate(ot[i].prefab, _hit.point, Quaternion.identity);
                    Debug.Log(_hit.point);   
                }
                j++;
            }
        }
    }
}
[System.Serializable]
public struct ObjectType
{
    public GameObject prefab;
    public float minDistance;
    public float maxDistance;
    public int amount;
}
//private void GenerateRandom()
//{
//    for (int i = 0; i < ot.Length; i++)
//    {
//        Vector3 fwd = this.transform.TransformDirection(Vector3.up);
//        if (Physics.Raycast(this.transform.position, fwd, out _hit, 100))
//        {
//            if (_hit.distance < 100 && _hit.distance > 50)
//            {
//                this.transform.localPosition = new Vector3(Random.Range(1, 239), -50, Random.Range(1, 239));
//                Debug.DrawRay(this.transform.position, fwd * 50, Color.green);
//                GameObject go = Instantiate(cube, _hit.point, Quaternion.identity);
//                Debug.Log(_hit.point);

//            }
//        }
//    }

//}