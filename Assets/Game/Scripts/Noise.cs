using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Noise
{
    public static float[,] GenerateNoiseMap(int mapWidth,int mapHeight,float scale,int seed, int octaves, float persistant, float lacunarity , Vector2 offset, bool flattenArea, float flatHeight)
    {
        float[,] noiseMap = new float[mapWidth,mapHeight];

        System.Random prng = new System.Random(seed);
        Vector2[] octaveOffset = new Vector2[octaves];
        for (int i = 0; i < octaves; i++)
        {
            float offsetX = prng.Next(-100000,100000) + offset.x;
            float offsetY = prng.Next(-100000,100000) + offset.y;
            octaveOffset[i] =  new Vector2(offsetX, offsetY);
        }


        if (scale<=0)
        {
            scale =0.0001f;
        }
        float maxNoiseHeight = float.MinValue;
        float minNoiseHeight = float.MaxValue;

        float halfWidth = mapWidth / 2f;
        float halfHeight = mapHeight / 2f;


        for (int y=0;y<mapHeight ;y++)
        {
            for (int x = 0;x<mapWidth ;x++)
            {
                float amplitude = 1;
                float frequency = 1;
                float noiseHeight = 0;


                for (int i = 0;i<octaves ; i++) 
                {
                float sampleX = (x - halfWidth + octaveOffset[i].x) /scale *frequency;
                float sampleY = (y - halfHeight + octaveOffset[i].y) /scale * frequency;

                float perlinValue = Mathf.PerlinNoise(sampleX,sampleY) *2 -1;

                noiseMap[x,y] = perlinValue;
                noiseHeight += perlinValue * amplitude;
                
                amplitude *= persistant;
                frequency *= lacunarity;

                }

                if (noiseHeight >maxNoiseHeight)
                {
                    maxNoiseHeight = noiseHeight;
                }
                else if (noiseHeight < minNoiseHeight)
                {
                    minNoiseHeight = noiseHeight;
                }

                noiseMap[x,y] = noiseHeight;
            }
        }

        for (int y = 0; y < mapHeight; y++)
        {
            for (int x = 0; x < mapWidth; x++)
            {
                
                noiseMap[x, y] = Mathf.InverseLerp(minNoiseHeight,maxNoiseHeight, noiseMap[x,y]);
                if (flattenArea && noiseMap[x, y] <flatHeight)
                {
                    noiseMap[x, y] = flatHeight;
                }
            }
        }
        return noiseMap;
    }

    
}
