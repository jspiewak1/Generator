using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeCreator : MonoBehaviour
{

    private GameObject cube;
    public int size = 5;
    public Vector3 offset;
    private void Start()
    {
        offset = transform.position;
        for (int x =0;x< size; x++)
        {
            for (int y = 0; y < size; y++)
            {
                VoxelCreators.MakeCube(new Vector3(x, getRand(),  y) + offset, 1);
            }
        }
    }
    private float getRand()
    {
        return Random.Range(0, 2);
    }
}
