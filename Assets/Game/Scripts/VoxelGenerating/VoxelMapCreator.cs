using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VoxelMapCreator : MonoBehaviour
{
    private GameObject cube;
    private Vector3 _center;
    [SerializeField] private int _width;
    //[SerializeField] private int _height;
    [SerializeField] private int _mapScale;
    [SerializeField] private float _mapHeightMultiplier; //ten prawdziwy mapHeight
    [SerializeField] private AnimationCurve meshHeightCurve;
    [SerializeField] private bool _isflattening;
    [Range(0, 1)]
    [SerializeField] private float _flatLevel;

    [SerializeField] private int _octaves;
    [Range(0, 1)]
    [SerializeField] private float _persistance;
    [SerializeField] private float _lacuranity;


    [SerializeField] private int _seed;
    [SerializeField] private Vector2 _offset;

    [Range(1,64)]
    [SerializeField] private int maximumMapHeight;


    private void Start()
    {
        float[,] noiseMap = Noise.GenerateNoiseMap(_width, _width, _mapScale, _seed, _octaves, _persistance, _lacuranity, _offset, _isflattening, _flatLevel);
        //float[,] noiseMap = new float[_width, _height];
        for (int x=0;x<_width ;x++)
        {
            for (int y = 0; y < _width; y++)
            {
                noiseMap[x, y] = Mathf.Round(Mathf.Lerp(0, maximumMapHeight, noiseMap[x, y]));
            }
        }

        Debug.Log(noiseMap.Length);
         for (int x = 0; x < _width; x++)
        {
            for (int y = 0; y < _width; y++)
            {
                VoxelCreators.MakeCube(new Vector3(x, noiseMap[x, y], y), 1);
            }
        }
    }
}
