using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using UnityEngine;

public class VoxelCreators : MonoBehaviour
{
    private static GameObject _cubePrefab;
    private static GameObject _cubeContainer;
    
    private static List<GameObject> _cubes = new List<GameObject>();

    public static GameObject MakeCube(Vector3 position, float size)
    {
        if (_cubeContainer == null)
        {
            _cubeContainer = new GameObject("cube container");
            _cubes = new List<GameObject>();
        }
        
        GameObject cube = Instantiate(CubePrefab()) as GameObject;
        _cubes.Add(cube);
        cube.transform.position = position;
        cube.transform.parent = _cubeContainer.transform;
        cube.name = "cube " + _cubes.Count;

        //cube.GetComponent<Renderer>().material.color = ;
        cube.transform.localScale = new Vector3(size, size, size);

        return cube;
    }
    private static GameObject CubePrefab()
    {
        if (_cubePrefab == null)
        {
           _cubePrefab = GameObject.CreatePrimitive(PrimitiveType.Cube);
        }
        return _cubePrefab;
    }
}
