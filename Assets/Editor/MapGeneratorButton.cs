using System.Collections;
using UnityEditor;
using UnityEngine;


[CustomEditor(typeof(MapGenerator))]
public class MapGeneratorButton :Editor
{
    public override void OnInspectorGUI()
    {
        MapGenerator mapGen = (MapGenerator)target;

        if (DrawDefaultInspector())
        {
            if (mapGen.noiseUpdate)
            {
                mapGen.GenerateNoiseMap();

            }
        }
        //DrawDefaultInspector();
        if (GUILayout.Button("Generate"))
        {
            mapGen.GenerateNoiseMap(); 
        }
    }
}
